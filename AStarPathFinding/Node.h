#ifndef NODE_H
#define NODE_H


class Node {
private:
	//m_gCost is distance to startNode, m_hCost is distance to targetNode.
	int m_gCost;
	int m_hCost;
	int m_parentX;
	int m_parentY;
	int m_positionX;
	int m_positionY;



public:

	Node();
	Node(const int positionX, const int positionY);
	
	//fCost is the likelyhood of this node being part of the shortest path.
	int getFCost() { return m_gCost + m_hCost; }
	int getHCost() { return m_hCost; }
	int getPositionX() { return m_positionX; }
	int getPositionY() { return m_positionY; }
	int getParentX() { return m_parentX; }
	int getParentY() { return m_parentY; }

	void SetPosition(const int positionX, const int positionY);
	void SetGCost(const int gCost);
	void SetHCost(const int hCost);
	void SetParentCoordinates(const int x, const int y);

};
#endif