#include "stdafx.h"
#include "SearchPath.h"
#include "Node.h"
#include <vector>
#include <iostream>
#include <memory>
#include <mutex>


SearchPath::SearchPath() {}

int FindPath(const int nStartX, const int nStartY, const int nTargetX, const int nTargetY, const unsigned char* pMap, const int nMapWidth, const int nMapHeight, int* pOutBuffer, const int nOutBufferSize);

void RunPathFinding(const int nStartX, const int nStartY, const int nTargetX, const int nTargetY, const unsigned char* pMap, const int nMapWidth, const int nMapHeight, int* pOutBuffer, const int nOutBufferSize, const int increaseBufferSize) {

	//Total amount of steps in the shortest path.
	int steps = 0;

	//Make the output from this class thread-safe
	static std::mutex io_mutex;
	std::lock_guard<std::mutex> lk(io_mutex);


	steps = FindPath(nStartX, nStartY, nTargetX, nTargetY, pMap, nMapWidth, nMapHeight, pOutBuffer, nOutBufferSize);

	if (steps > nOutBufferSize) {
		if (increaseBufferSize != 0) {
			//The user has decided to re-run the program with a bigger nOutBufferSize.
			std::cout << "The user has chosen to automatically re-run the program with a larger 'nOutBufferSize'." << std::endl;
			pOutBuffer = new int[nOutBufferSize + increaseBufferSize];
			int newOutBufferSize = nOutBufferSize + increaseBufferSize;
			steps = FindPath(nStartX, nStartY, nTargetX, nTargetY, pMap, nMapWidth, nMapHeight, pOutBuffer, newOutBufferSize);
		}

	}

	std::cout << "The number of steps for shortest path: " << steps << std::endl;
	delete[] pOutBuffer;
}

int FindPath(const int nStartX, const int nStartY, const int nTargetX, const int nTargetY, const unsigned char* pMap, const int nMapWidth, const int nMapHeight, int* pOutBuffer, const int nOutBufferSize) {
	
	std::vector<std::vector<Node>> grid;
	
	grid.resize(nMapHeight, std::vector<Node>(nMapWidth));

	//Vector to keep track of registered neighbours that we haven't tested.
	std::vector<std::shared_ptr<Node>> openSet;
	//Vector to keep track of registered neighbours that we have tested.
	std::vector<std::shared_ptr<Node>> closedSet;

	for (int y = 0; y < nMapHeight; y++) {
		for (int x = 0; x < nMapWidth; x++) {
			grid[y][x].SetPosition(x, y);
		}
	}

	//gCost is the distance to the startnode.
	grid[nStartY][nStartX].SetGCost(0);
	//hCost is the distance to the targetNode.
	grid[nStartY][nStartX].SetHCost(std::abs(nStartX - nTargetX) + std::abs(nStartY - nTargetY));

	std::shared_ptr<Node> startNode = std::make_shared<Node>(grid[nStartY][nStartX]);
	std::shared_ptr<Node> targetNode = std::make_shared<Node>(grid[nTargetY][nTargetX]);

	openSet.push_back(startNode);

	int currentX;
	int currentY;

	//Define the memberfunctions of Node so that we can use smart pointers in our vectors and run memberfunctions directly from them.
	auto getFCost = &Node::getFCost;
	auto getHCost = &Node::getHCost;
	auto getPositionX = &Node::getPositionX;
	auto getPositionY = &Node::getPositionY;

	while (!openSet.empty()) {
		//We need to define a new currentNode to compare it with the other Nodes inside our openSet.
		std::shared_ptr<Node> currentNode = openSet.front();

		int currentNodeIndex = 0;

		//Compare the neighbours registered in openSet to see which node is the most likely to be a part of the shortest path.
		for (int i = 0; i < openSet.size(); i++) {
			//fCost is the likelyhood of that node being a part of the shortest path.
			int openNodeFCost = openSet[i]->getFCost();
			int currentFCost = currentNode->getFCost();
			if (openNodeFCost <= currentFCost) {
				if (openSet[i]->getHCost() < currentNode->getHCost() || openNodeFCost < currentFCost) {
					currentNode = openSet[i];
					currentNodeIndex = i;
				}
			}
		}
		
		currentX = currentNode->getPositionX();
		currentY = currentNode->getPositionY();

		openSet.erase(openSet.begin() + currentNodeIndex);
		closedSet.push_back(currentNode);

		if (currentX == nTargetX && currentY == nTargetY) {
			//The path from startNode to targetNode
			std::vector<std::shared_ptr<Node>> path;
			
			//Backtrack to the parentnode of each node along a path from targetNode to startNode.
			while ((currentX != nStartX) || (currentY != nStartY)) {
				path.push_back(currentNode);

				int parentY = grid[currentY][currentX].getParentY();
				int parentX = grid[currentY][currentX].getParentX();

				currentNode = std::make_shared<Node>(grid[parentY][parentX]);

				currentX = currentNode->getPositionX();
				currentY = currentNode->getPositionY();
			}

			//We need to reverse so that path starts with the startNode instead of the targetNode.
			std::reverse(path.begin(), path.end());

			int pathSize = path.size();

			//We only want to print the Nodes of the shortest path to pOutBuffer if we now that there is enough space for them.
			if (pathSize <= nOutBufferSize) {
				std::cout << "The outBuffer: {";
				for (int i = 0; i < pathSize; i++) {
					pOutBuffer[i] = path[i]->getPositionX() + (path[i]->getPositionY() * nMapWidth);
					if (i == pathSize - 1) {
						std::cout << pOutBuffer[i];
					}
					else {
						std::cout << pOutBuffer[i] << ", ";
					}

				}
				std::cout << " }" << std::endl;
			}
			else {
				std::cout << "The shortest path is longer than 'nOutBufferSize'." << std::endl;
			}

			return pathSize;
		}

		std::vector<std::shared_ptr<Node>> neighbours;

		//We iterate through every neighbour of the current Node inside the grid to find the walkable nodes.
		for (int y = -1; y <= 1; y++) {
			for (int x = -1; x <= 1; x++) {
				if (x == 0 && y == 0) {
					continue;
				}
				//Ignore the diagonal neighbours.
				if (x == 0 || y == 0) {
					int checkX = currentNode->getPositionX() + x;
					int checkY = currentNode->getPositionY() + y;

					if (checkX >= 0 && checkX < nMapWidth && checkY >= 0 && checkY < nMapHeight) {
						//If the value of the node is '1' inside pMap the node is walkable.
						//We can safely assume that both startNode and targetNode is walkable.
						if (pMap[checkX + (checkY * nMapWidth)] == '1') {
							neighbours.push_back(std::make_shared<Node>(grid[checkY][checkX]));
						}
					}
				}
			}
		}
		
		//Iterate through the neighbours of walkable nodes to check if we have tested them, added them to openSet already or if we will add them to openSet now.
		for each (std::shared_ptr<Node> n in neighbours) {

			

			int neighbourX = n->getPositionX();
			int neighbourY = n->getPositionY();

			//If true, we don't want to add this neighbour to openSet.
			bool goToNextNode = false;

			for (int i = 0; i < closedSet.size(); i++) {
				if (closedSet[i]->getPositionX() == neighbourX && closedSet[i]->getPositionY() == neighbourY) {
					goToNextNode = true;
					break;
				}
			}

			if (goToNextNode) {
				continue;
			}

			bool existInOpenSet = false;
				for (int i = 0; i < openSet.size(); i++) {
					if (openSet[i]->getPositionX() == neighbourX && openSet[i]->getPositionY() == neighbourY) {
						goToNextNode = true;
						break;
					}
				}


			if (goToNextNode) {
				continue;
			}

			//Adding the current neighbour to openSet.
			grid[neighbourY][neighbourX].SetGCost(std::abs(nStartX - neighbourX) + std::abs(nStartY - neighbourY));
			grid[neighbourY][neighbourX].SetHCost(std::abs(nTargetX - neighbourX) + std::abs(nTargetY - neighbourY));

			grid[neighbourY][neighbourX].SetParentCoordinates(currentX, currentY);

			openSet.push_back(std::make_shared<Node>(grid[neighbourY][neighbourX]));


		}
	}

	//There is no walkable path from start to end.
	return -1;

}
