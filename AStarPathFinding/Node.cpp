#include "stdafx.h"
#include "Node.h"



Node::Node() {}

Node::Node(const int positionX, const int positionY) {
	m_positionX = positionX;
	m_positionY = positionY;
}

void Node::SetPosition(const int positionX, const int positionY) {
	m_positionX = positionX;
	m_positionY = positionY;
}

void Node::SetGCost(const int gCost) {
	m_gCost = gCost;
}

void Node::SetHCost(const int hCost) {
	m_hCost = hCost;
}

void Node::SetParentCoordinates(const int x, const int y) {
	m_parentX = x;
	m_parentY = y;
}