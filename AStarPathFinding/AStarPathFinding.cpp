// AStarPathFinding.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Node.h"
#include "SearchPath.h"
#include <iostream>
#include <mutex>

void RunSetup();
void RunPathFinding(const int nStartX, const int nStartY, const int nTargetX, const int nTargetY, const unsigned char* pMap, const int nMapWidth, const int nMapHeight, int* pOutBuffer, const int nOutBufferSize, const int increaseBufferSize);

int main()
{
	RunSetup();

	return 0;
}

//Let the user input the values for the grid and the variables used to run this program and to test the shortest path of a given 2d-grid.
void RunSetup() {
	static std::mutex io_mutex;
	std::lock_guard<std::mutex> lk(io_mutex);
	static int gridWidth;
	static int gridHeight;
	static int startX;
	static int startY;
	static int targetX;
	static int targetY;

	static int outBufferSize;
	static int increaseBufferSize;
	std::cout << "Enter an integer value that is higher or equal to 1, to set grid width: ";
	std::cin >> gridWidth;
	while (gridWidth < 1) {
		std::cout << "You have entered an integer that is too low. Please enter a correct integer value." << std::endl;
		std::cout << "Enter an integer value that is higher or equal to 1, to set grid width: ";
		std::cin >> gridWidth;
	}
	std::cout << "Enter an integer value that is higher or equal to 1, to set grid height: ";
	std::cin >> gridHeight;
	while (gridHeight < 1) {
		std::cout << "You have entered an integer that is too low. Please enter a correct integer value." << std::endl;
		std::cout << "Enter an integer value that is higher or equal to 1, to set grid height: ";
		std::cin >> gridHeight;
	}
	std::cout << "Enter an integer value to set the x-value of the starting point inside the grid" << std::endl;
	std::cout << "(Must be less than " << gridWidth << " and equal to or higher than 0: ";
	std::cin >> startX;
	while (startX < 0 || startX > gridWidth) {
		std::cout << "You have entered an integer that is either too high or too low. Please enter a correct integer value." << std::endl;
		std::cout << "You must again enter an integer value to set the x-value of the starting point inside the grid" << std::endl;
		std::cout << "(Must be less than " << gridWidth << " and equal to or higher than 0: ";
		std::cin >> startX;
	}
	std::cout << "Enter an integer value to set the y-value of the starting point inside the grid" << std::endl;
	std::cout << "(Must be less than " << gridHeight << " and equal to or higher than 0: ";
	std::cin >> startY;
	while (startY < 0 || startY > gridHeight) {
		std::cout << "You have entered an integer that is either too high or too low. Please enter a correct integer value." << std::endl;
		std::cout << "You must again enter an integer value to set the y-value of the starting point inside the grid" << std::endl;
		std::cout << "(Must be less than " << gridHeight << " and equal to or higher than 0: ";
		std::cin >> startY;
	}
	std::cout << "Enter an integer value to set the x-value of the target point inside the grid" << std::endl;
	std::cout << "(Must be less than " << gridWidth << " and equal to or higher than 0: ";
	std::cin >> targetX;
	while (targetX < 0 || targetX > gridWidth) {
		std::cout << "You have entered an integer that is either too high or too low. Please enter a correct integer value." << std::endl;
		std::cout << "You must again enter an integer value to set the x-value of the target point inside the grid" << std::endl;
		std::cout << "(Must be less than " << gridWidth << " and equal to or higher than 0: ";
		std::cin >> targetX;
	}
	std::cout << "Enter an integer value to set the y-value of the target point inside the grid" << std::endl;
	std::cout << "(Must be less than " << gridHeight << " and equal to or higher than 0: ";
	std::cin >> targetY;
	while (targetY < 0 || targetY > gridHeight) {
		std::cout << "You have entered an integer that is either too high or too low. Please enter a correct integer value." << std::endl;
		std::cout << "You must again enter an integer value to set the y-value of the target point inside the grid" << std::endl;
		std::cout << "(Must be less than " << gridHeight << " and equal to or higher than 0: ";
		std::cin >> targetY;
	}
	std::cout << "Enter the value '1' or '0' for each square in the grid. Must be the exact same amount as" << gridWidth * gridHeight << std::endl;
	std::cout << "(1 means the square is walkable, 0 means that there is an obstacle). (Each square will be presented with it's 'x,y' coordinates)." << std::endl;
	unsigned char *pMap = new unsigned char[gridWidth * gridHeight];
	int i = 0;
	for (int y = 0; y < gridHeight; y++) {
		for (int x = 0; x < gridWidth; x++) {
			std::cout << "Enter the attribute 'walkable'(1 == true, 0 == false) to square " << x << "," << y << ": ";
			std::cin >> pMap[i];
			while (pMap[i] != '1' && pMap[i] != '0')
			{
				std::cout << "You have entered an illegal character. Please enter only 1 or 0." << std::endl;
				std::cout << "Enter the attribute 'walkable'(1 == true, 0 == false) to square " << x << "," << y << ": ";
				std::cin >> pMap[i];
				std::cout << pMap[i] << std::endl;
			}
			i++;
		}
	}
	std::cout << "Enter an integer value to set the maximum amount of steps that the shortest path is allowed to take: ";
	std::cin >> outBufferSize;
	std::cout << "Enter an integer value to set how much the maximum amount of steps that the shortest path is allowed to take will be" << std::endl;
	std::cout << "by if the shortest path is longer than that amount of steps." << std::endl;
	std::cout << "(0 means that you want the script to be canceled if the shortest path is longer than the maximum amount of steps you've entered):";
	std::cin >> increaseBufferSize;
	int* outBuffer = new int[outBufferSize];
	const unsigned char* newPMap = reinterpret_cast<const unsigned char *>(pMap);
	
	RunPathFinding(startX, startY, targetX, targetY, newPMap, gridWidth, gridHeight, outBuffer, outBufferSize, increaseBufferSize);

}

