#ifndef SearchPath_H
#define SearchPath_H

class SearchPath {
private:
	int FindPath(const int nStartX, const int nStartY, const int nTargetX, const int nTargetY, const unsigned char* pMap, const int nMapWidth, const int nMapHeight, int* pOutBuffer, const int nOutBufferSize);


public:
	SearchPath();

	void RunPathFinding(const int nStartX, const int nStartY, const int nTargetX, const int nTargetY, const unsigned char* pMap, const int nMapWidth, const int nMapHeight, int* pOutBuffer, const int nOutBufferSize, const int increaseBufferSize);


};

#endif